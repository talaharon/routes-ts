import express, { NextFunction, Request, RequestHandler, Response } from "express";
import log from "@ajar/marker";
import morgan from "morgan";
import searchRouter from "./search.js";
import generalRouter from "./routes.js";

const { PORT, HOST = "localhost"} = process.env;

// const timeLogger: RequestHandler = (req, res, next) => {
//     console.log(new Date());
//     next();
// };
const timeLogger = (req: Request, res: Response, next: NextFunction) => {
    console.log(new Date());
    next();
};

const app = express();
app.use(morgan("dev"));
app.use(express.json());
app.use(timeLogger);

app.use("/", generalRouter);
app.use("/search", searchRouter);

app.use("*", (req, res) => {
    res.status(404).send("Error 404: Route does not exist");
});

app.listen(Number(PORT), HOST, () => {
    log.magenta("🌎  listening on", "http://",HOST,":",PORT);
});

