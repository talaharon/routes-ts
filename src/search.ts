import express, { RequestHandler } from "express";

const router = express.Router();

const searchLogger:RequestHandler = (req, res, next) => {
    console.log("Search was activated!");
    next();
};

router.use(searchLogger);

//Example: '/search?food=burger&town=ashdod'
router.get("/", (req, res) => {
    res.status(200).set("Content-Type", "text/json").send(req.query);
});

//Example: '/search/42/books/92'
router.get("/:userId/books/:bookId", (req, res) => {
    const htmlString = `<h1>User ID: ${req.params.userId}</h1>
      <h1>Book ID: ${req.params.bookId}</h1>`;
    res.status(200).set("Content-Type", "text/html").send(htmlString);
});

export default router;
