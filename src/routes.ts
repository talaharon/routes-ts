import express, { RequestHandler } from "express";

const router = express.Router();
const userLogger:RequestHandler = (req, res, next) => {
    console.log("User route was activated!");
    next();
};

router.get("/", (req, res) => {
    res.status(200).send("Hello Express!");
});

router.get("/users", userLogger, (req, res) => {
    res.status(200).send("Get all Users");
});

//Example: '/weather' with '{"Temperature": "19°","Overview": "Cloudy"} as body
router.get("/weather", (req, res) => {
    console.log(req.body);
    res.status(200).set("Content-Type", "text/json").send(req.body);
});

export default router;
